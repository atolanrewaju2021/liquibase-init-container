package az.ingress.sampleproject.service;

import az.ingress.sampleproject.dto.EmployeeRequestDto;
import az.ingress.sampleproject.dto.EmployeeResponseDto;

public interface EmployeeService {

    EmployeeResponseDto createEmployee(EmployeeRequestDto employeeDto);

    EmployeeResponseDto getEmployeeById(Long id);

}
